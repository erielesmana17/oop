    <?php
    
    require_once('animal.php');
    require_once('Ape.php');
    require_once('Frog.php');
    $sheep = new Animal("Shaun");
    echo "Name = " . $sheep->name ."<br>";
    echo "Legs = " . $sheep->legs ."<br>";
    echo "Cold Blooded = " . $sheep->cold_blooded ."<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name = " . $sungokong->name ."<br>";
    echo "Legs = " . $sungokong->legs ."<br>";
    echo "Cold Blooded = " . $sungokong->cold_blooded ."<br>";
    echo "Yell = ";
    echo $sungokong->yell() ."<br><br>";

    $kodok = new frog("buduk");
    echo "Name = " . $kodok->name ."<br>";
    echo "Legs = " . $kodok->legs ."<br>";
    echo "Cold Blooded = " . $kodok->cold_blooded ."<br>";
    echo "Jump = ";
    echo $kodok->jump();


    ?>

